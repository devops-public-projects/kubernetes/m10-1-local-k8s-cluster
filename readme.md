
# Deploy MongoDB and Mongo Express into a local K8s cluster

## Technologies Used
- Kubernetes
- Docker
- MongoDB
- Mongo-Express

## Project Description
- Setup local K8s cluster with Minikube
- Deploy MongoDB and Mongo-Express with configuration and credentials extracted into **ConfigMap** and **Secret**

## Prerequisites
- [Minikube](https://minikube.sigs.k8s.io/docs/start/) 
- [Kubectl](https://kubernetes.io/docs/tasks/tools/)
- [Docker](https://docs.docker.com/engine/install/)

## Guide Steps

**Note**: Depending on your Minikube install you can need to run the commands in the below syntax instead
`minikube kubectl get secret`
`minikube kubectl -- apply -f deploy.yaml`

### Configure MongoDB Yaml
- We need to find the necessary configuration for MongoDB. This can be found from Docker Hub at [MongoDB Docker Configuration](https://hub.docker.com/_/mongo)
	- The default port is 27017
	- Root Username and Password environment variables:
		- MONGO_INITDB_ROOT_USERNAME
		- MONGO_INITDB_ROOT_PASSWORD

### Create a Secret
- With the [base Secret yaml file](https://kubernetes.io/docs/concepts/configuration/secret/) we will add a **mongo-root-username** and **mongo-root-password** key-value pair.
	- Each entry needs to be a base64 encoded value. This can be achieved by running this command on your computers terminal.
		- `echo -n 'SECRET_VALUE' | base64`
		- **Note**: More secure techniques will be used later, don't use this method in production!
- Apply the Secret
	-  `kubectl apply -f monogo-secret.yaml`
- Verify Secret
	- `kubectl get secret`

### Finalize MongoDB Yaml
- For our two secrets, we will set them to reference our new **Secret**
```yaml
valueFrom:
	secretKeyRef:
		name: mongodb-secret
		key: mongo-root-XXXXXX
```
- Create the Deployment
	- `kubectl apply -f mongo.yaml`


![Successful MongoDB Pod Start](/images/m10-1-successful-mongodb-start.png)

### Configure MongoDB Internal Service
- We'll utilize the same `mongo.yaml` since the **Service** is for the same application. [Sample K8s MongoDB Service Yaml](https://phoenixnap.com/kb/kubernetes-mongodb).
- We will connect the Service to the Deployment by referencing the Deployment label `mongodb` and we'll set the ports.
```yaml
spec:
	selector:
		app: mongodb
	ports:
	-	protocol: TCP
		port: 27017
		targetPort: 27017
```
- Apply the update
	- `kubectl apply -f mongo.yaml`
- Verify the service is attached to the **Pod** correctly
	- `kubectl describe service mongodb-service`
- Verify **Pod** IP
	- `kubectl get pod -o wide`


![Service Created](/images/m10-1-service-created.png)

### Create Mongo-Express Deployment
- You can find the configuration requirements here [Mongo-Express Docker Configuration](https://hub.docker.com/_/mongo-express)
- The values will be the same as the `mongo.yaml` since we are referencing the same credentials for both. The port is obtained from the Mongo-Express Docker page.
```yaml
- containerPort: 8081
valueFrom:
	secretKeyRef:
		name: mongodb-secret
		key: mongo-root-XXXXXX
```

### Create External Configuration
[Sample ConfigMap.yaml](https://kubernetes.io/docs/tasks/configure-pod-container/configure-pod-configmap/)
- We'll use the sample to fill in our needed data, which is just the **database_url** which is equal to our MongoDB service name.
```yaml
apiVersion: v1
kind: ConfigMap
metadata:
	name: mongodb-configmap
data:
	database_url: "mongodb-service"
```

- Apply the ConfigMap
	- `kubectl apply -f mongo-configmap.yaml`


![Config Map Applied](/images/m10-1-applied-configmap.png)

### Finalize Mongo-Express Deployment
- We will add a final environment variable to our `mongo-express.yaml`
```yaml
- name: ME_CONFIG_MONGODB_SERVER
  valueFrom:
	  configMapKeyRef:
		  name: mongodb-configmap 
		  key: database_url
```
- Apply Mongo-Express
	- `kubectl apply -f mongo-express.yaml`


![Mongo Express Applied](/images/m10-1-mongo-express-running.png)


### Setup External Service for Mongo Express
Finally, we want to access Mongo Express via the web browser. We will achieve this by configuring an external **Service**. [Sample Yaml files can be found here](https://github.com/weechien/mongo-express-k8s).
```yaml
apiVersion: v1
kind: Service
metadata:
	name: mongo-express-service
spec:
	selector:
		app: mongo-express
	type: LoadBalancer
	ports:
	-	protocol: TCP
		port: 8081
		targetPort: 8081
		nodePort: 30000
```
- Apply Mongo-Express Service
	- `kubectl apply -f mongo-express.yaml`
- Verify Service Configuration
	- `kubectl get service`


![Mongo Express Service Started](/images/m10-1-mongo-express-service-started.png)

- Access Mongo-Express via the external IP
	- `minikube service mongo-express-service`
	- A web browser will open and allow you to log in to Mongo-Express

![Mongo Express Accessible](/images/m10-1-mongo-express-accessible.png)